package pl.pwlek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloGitApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloGitApplication.class, args);
		
		System.out.println("Siema GIT");
		
		System.out.println("To jest wersja druga systemu");
		
		System.out.println("To jest jeszcze jedna wersja systemu :/");
	}

}
